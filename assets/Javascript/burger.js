window.onload = () => {
        
    //buger animation 

    function burgerMenu(){ 
        const burgers = document.querySelector('.burger')
        const menu = document.querySelector('.menu')
        let menuOpen = false 

        burgers.addEventListener('click', ()=> { 
            if(!menuOpen) { 
                burgers.classList.add("open")
                menu.classList.remove("close")
                menuOpen = true
            } else { 
                burgers.classList.remove("open")
                menu.classList.add("close")
                menuOpen = false
            }
        })
    }
    burgerMenu()

};
