
    const form = document.querySelector('form')
        form.addEventListener('submit', function (e){
            e.preventDefault();
            fetch(this.action, {
                body: new FormData(e.target),
                method: 'POST'
            })
            .then(response => response.json())
            .then(json => {
                handleResponse(json);
            });
        })

        const handleResponse = function (response) {
            switch(response.contact) {
                case 'CONTACT__ADDED__SUCCESS':
                    break;
                case 'CONTACT__ADDED__INVALIDE':
                    handleErrors(response.errors);
                    break;
            }
        }
