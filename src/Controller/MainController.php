<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Contact;
use App\Entity\Project;
use App\Form\ContactType;
use Symfony\Component\Mime\Email;
use App\Entity\CategoryCompetence;
use App\Repository\ArticleRepository;
use App\Repository\ProjectRepository;
use App\Repository\CompetenceRepository;
use App\Repository\CustomPageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryCompetenceRepository;
use App\Repository\ServicesRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    public function __construct(

       private Environment $environment,
     
   ) {}


    #[Route('/', name: 'app_main')]
    public function index(Request $request ,EntityManagerInterface $manager,CategoryCompetenceRepository $categoryCompetenceRepository, CompetenceRepository $competenceRepository, ProjectRepository $projectRepository,ArticleRepository $articleRepository, MailerInterface $mailer, CustomPageRepository $customPageRepository,ServicesRepository $servicesRepository): Response
    {


        $contact = new Contact(); 
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact =$form->getData();
            $manager->persist($contact);
            $manager->flush();

        //     return new JsonResponse([
        //    'contact' => Contact::CONTACT__ADDED__SUCCESS,
        //     'html' => $this->environment->render('main/index.html.twig')
        //     ]);


            $email = (new Email())
            ->from('hello@example.com')
            ->to('you@example.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');
            
            $mailer->send($email);

            $this->addFlash('success', 'votre message a été envoyé avec succès');
            return $this->redirectToRoute('app_main');
        }


        $articles = $articleRepository->findAll() ; 
        $categCompetence = $categoryCompetenceRepository->findAll();
        $competencesByCategorylanguage = $competenceRepository->findByCategory('Language');
        $competencesByCategoryFrameWork = $competenceRepository->findByCategory('Framework');
        $logiciels =$competenceRepository->findByCategory('Logiciels');
        $projects = $projectRepository->findAll();
        $pages = $customPageRepository->findAll(); 
        $services = $servicesRepository->findAll();


 

        return $this->render('main/index.html.twig', [
            'categCompetence' => $categCompetence,
            'languages' => $competencesByCategorylanguage,
            'Frameworks' => $competencesByCategoryFrameWork,
            'projects' => $projects,
            'articles' => $articles,
            'form' => $form->createView(),
            'pages' => $pages,
            'services'=> $services, 
            'Logiciels' => $logiciels
        ]);
    }
}
