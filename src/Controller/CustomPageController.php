<?php

namespace App\Controller;

use App\Repository\CustomPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomPageController extends AbstractController
{
    #[Route('/page/{url}', name: 'app_custom_page')]
    public function index(CustomPageRepository $customPageRepository, $url): Response
    {
        $page = $customPageRepository->findOneBy(['url'=>$url]);
        return $this->render('custom_page/index.html.twig', [
            'page' => $page
        ]);
    }
}
