<?php

namespace App\Controller\Admin;

use App\Entity\CategoryCompetence;
use App\Form\CategoryCompetenceType;
use App\Repository\CategoryCompetenceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adminweb/categorycompetence')]
class AdminWebCategoryCompetenceController extends AbstractController
{
    #[Route('/', name: 'app_admin_web_category_competence_index', methods: ['GET'])]
    public function index(CategoryCompetenceRepository $categoryCompetenceRepository): Response
    {
        return $this->render('admin_web_category_competence/index.html.twig', [
            'category_competences' => $categoryCompetenceRepository->findAll(),
            'active' => 'category_competences'
        ]);
    }

    #[Route('/new', name: 'app_admin_web_category_competence_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoryCompetenceRepository $categoryCompetenceRepository): Response
    {
        $categoryCompetence = new CategoryCompetence();
        $form = $this->createForm(CategoryCompetenceType::class, $categoryCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryCompetenceRepository->add($categoryCompetence, true);

            return $this->redirectToRoute('app_admin_web_category_competence_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_category_competence/new.html.twig', [
            'category_competence' => $categoryCompetence,
            'form' => $form,
            'active' => 'category_competences'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_competence_show', methods: ['GET'])]
    public function show(CategoryCompetence $categoryCompetence): Response
    {
        return $this->render('admin_web_category_competence/show.html.twig', [
            'category_competence' => $categoryCompetence,
            'active' => 'category_competences'

        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_web_category_competence_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategoryCompetence $categoryCompetence, CategoryCompetenceRepository $categoryCompetenceRepository): Response
    {
        $form = $this->createForm(CategoryCompetenceType::class, $categoryCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryCompetenceRepository->add($categoryCompetence, true);

            return $this->redirectToRoute('app_admin_web_category_competence_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_category_competence/edit.html.twig', [
            'category_competence' => $categoryCompetence,
            'form' => $form,
            'active' => 'category_competences'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_competence_delete', methods: ['POST'])]
    public function delete(Request $request, CategoryCompetence $categoryCompetence, CategoryCompetenceRepository $categoryCompetenceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryCompetence->getId(), $request->request->get('_token'))) {
            $categoryCompetenceRepository->remove($categoryCompetence, true);
        }

        return $this->redirectToRoute('app_admin_web_category_competence_index', [], Response::HTTP_SEE_OTHER);
    }
}
