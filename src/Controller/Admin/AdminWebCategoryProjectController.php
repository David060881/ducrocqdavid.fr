<?php

namespace App\Controller\Admin;


use App\Entity\CategoryProject;
use App\Form\CategoryProjectType;
use App\Repository\CategoryProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adminweb/categoryproject')]
class AdminWebCategoryProjectController extends AbstractController
{
    #[Route('/', name: 'app_admin_web_category_project_index', methods: ['GET'])]
    public function index(CategoryProjectRepository $categoryProjectRepository): Response
    {
        return $this->render('admin_web_category_project/index.html.twig', [
            'category_projects' => $categoryProjectRepository->findAll(),
            'active' => 'category_projects'
        ]);
    }

    #[Route('/new', name: 'app_admin_web_category_project_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoryProjectRepository $categoryProjectRepository, EntityManagerInterface $manager): Response
    {
        $categoryProject = new CategoryProject();
        $form = $this->createForm(CategoryProjectType::class, $categoryProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryProjectRepository->add($categoryProject, true);

            $categoryProject->setCreatedAt(new \DateTime());
            $categoryProject->setUpdatedAt(new \DateTime()); 
    
            $manager->persist($categoryProject);
            $manager->flush();

            return $this->redirectToRoute('app_admin_web_category_project_index', [], Response::HTTP_SEE_OTHER);
       
        }

    


        return $this->renderForm('admin_web_category_project/new.html.twig', [
            'category_project' => $categoryProject,
            'form' => $form,
            'active' => 'category_projects'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_project_show', methods: ['GET'])]
    public function show(CategoryProject $categoryProject): Response
    {
        return $this->render('admin_web_category_project/show.html.twig', [
            'category_project' => $categoryProject,
            'active' => 'category_projects'

        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_web_category_project_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategoryProject $categoryProject, CategoryProjectRepository $categoryProjectRepository): Response
    {
        $form = $this->createForm(CategoryProjectType::class, $categoryProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryProjectRepository->add($categoryProject, true);

            return $this->redirectToRoute('app_admin_web_category_project_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_category_project/edit.html.twig', [
            'category_project' => $categoryProject,
            'form' => $form,
            'active' => 'category_projects'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_project_delete', methods: ['POST'])]
    public function delete(Request $request, CategoryProject $categoryProject, CategoryProjectRepository $categoryProjectRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryProject->getId(), $request->request->get('_token'))) {
            $categoryProjectRepository->remove($categoryProject, true);
            
        }

        return $this->redirectToRoute('app_admin_web_category_project_index', [], Response::HTTP_SEE_OTHER);
    }
}
