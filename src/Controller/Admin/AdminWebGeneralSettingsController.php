<?php

namespace App\Controller\Admin;


use App\Entity\GeneralSettings;
use App\Form\GeneralSettingsType;
use App\Repository\GeneralSettingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adminWeb/generalSettings')]
class AdminWebGeneralSettingsController extends AbstractController
{
    #[Route('/', name: 'app_admin_web_general_settings_index', methods: ['GET'])]
    public function index(GeneralSettingsRepository $generalSettingsRepository): Response
    {
        return $this->render('admin_web_general_settings/index.html.twig', [
            'general_settings' => $generalSettingsRepository->findAll(),
            'active' => 'general_settings'
        ]);
    }

    #[Route('/new', name: 'app_admin_web_general_settings_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GeneralSettingsRepository $generalSettingsRepository): Response
    {
        $generalSetting = new GeneralSettings();
        $form = $this->createForm(GeneralSettingsType::class, $generalSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $generalSettingsRepository->add($generalSetting, true);

            return $this->redirectToRoute('app_admin_web_general_settings_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_general_settings/new.html.twig', [
            'general_setting' => $generalSetting,
            'form' => $form,
           

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_general_settings_show', methods: ['GET'])]
    public function show(GeneralSettings $generalSetting): Response
    {
        return $this->render('admin_web_general_settings/show.html.twig', [
            'general_setting' => $generalSetting,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_web_general_settings_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, GeneralSettings $generalSetting, GeneralSettingsRepository $generalSettingsRepository): Response
    {
        $form = $this->createForm(GeneralSettingsType::class, $generalSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $generalSettingsRepository->add($generalSetting, true);

            return $this->redirectToRoute('app_admin_web_general_settings_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_general_settings/edit.html.twig', [
            'general_setting' => $generalSetting,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_general_settings_delete', methods: ['POST'])]
    public function delete(Request $request, GeneralSettings $generalSetting, GeneralSettingsRepository $generalSettingsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$generalSetting->getId(), $request->request->get('_token'))) {
            $generalSettingsRepository->remove($generalSetting, true);
        }

        return $this->redirectToRoute('app_admin_web_general_settings_index', [], Response::HTTP_SEE_OTHER);
    }
}
