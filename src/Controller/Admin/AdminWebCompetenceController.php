<?php

namespace App\Controller\Admin;

use App\Entity\Competence;
use App\Form\CompetenceType;
use App\Repository\CompetenceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adminweb/competence')]
class AdminWebCompetenceController extends AbstractController
{
    #[Route('/', name: 'app_admin_web_competence_index', methods: ['GET'])]
    public function index(CompetenceRepository $competenceRepository): Response
    {
        return $this->render('admin_web_competence/index.html.twig', [
            'competences' => $competenceRepository->findAll(),
            'active'  => 'competences'
        ]);
    }

    #[Route('/new', name: 'app_admin_web_competence_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CompetenceRepository $competenceRepository): Response
    {
        $competence = new Competence();
        $form = $this->createForm(CompetenceType::class, $competence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $competenceRepository->add($competence, true);

            return $this->redirectToRoute('app_admin_web_competence_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_competence/new.html.twig', [
            'competence' => $competence,
            'form' => $form,
            'active'  => 'competences'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_competence_show', methods: ['GET'])]
    public function show(Competence $competence): Response
    {
        return $this->render('admin_web_competence/show.html.twig', [
            'competence' => $competence,
            'active'  => 'competences'

        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_web_competence_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Competence $competence, CompetenceRepository $competenceRepository): Response
    {
        $form = $this->createForm(CompetenceType::class, $competence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $competenceRepository->add($competence, true);

            return $this->redirectToRoute('app_admin_web_competence_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_competence/edit.html.twig', [
            'competence' => $competence,
            'form' => $form,
            'active'  => 'competences'

        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_competence_delete', methods: ['POST'])]
    public function delete(Request $request, Competence $competence, CompetenceRepository $competenceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$competence->getId(), $request->request->get('_token'))) {
            $competenceRepository->remove($competence, true);
        }

        return $this->redirectToRoute('app_admin_web_competence_index', [], Response::HTTP_SEE_OTHER);
    }
}
