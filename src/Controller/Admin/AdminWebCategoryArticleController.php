<?php

namespace App\Controller\Admin;

use App\Entity\CategoryArticle;
use App\Form\CategoryArticleType;
use App\Repository\CategoryArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adminweb/categoryarticle')]
class AdminWebCategoryArticleController extends AbstractController
{
    #[Route('/', name: 'app_admin_web_category_article_index', methods: ['GET'])]
    public function index(CategoryArticleRepository $categoryArticleRepository): Response
    {
        return $this->render('admin_web_category_article/index.html.twig', [
            'category_articles' => $categoryArticleRepository->findAll(),
            'active' => 'CategArticle'

        ]);
    }

    #[Route('/new', name: 'app_admin_web_category_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoryArticleRepository $categoryArticleRepository): Response
    {
        $categoryArticle = new CategoryArticle();
        $form = $this->createForm(CategoryArticleType::class, $categoryArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryArticleRepository->add($categoryArticle, true);

            return $this->redirectToRoute('app_admin_web_category_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_category_article/new.html.twig', [
            'category_article' => $categoryArticle,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_article_show', methods: ['GET'])]
    public function show(CategoryArticle $categoryArticle): Response
    {
        return $this->render('admin_web_category_article/show.html.twig', [
            'category_article' => $categoryArticle,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_web_category_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategoryArticle $categoryArticle, CategoryArticleRepository $categoryArticleRepository): Response
    {
        $form = $this->createForm(CategoryArticleType::class, $categoryArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryArticleRepository->add($categoryArticle, true);

            return $this->redirectToRoute('app_admin_web_category_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_web_category_article/edit.html.twig', [
            'category_article' => $categoryArticle,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_web_category_article_delete', methods: ['POST'])]
    public function delete(Request $request, CategoryArticle $categoryArticle, CategoryArticleRepository $categoryArticleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryArticle->getId(), $request->request->get('_token'))) {
            $categoryArticleRepository->remove($categoryArticle, true);
        }

        return $this->redirectToRoute('app_admin_web_category_article_index', [], Response::HTTP_SEE_OTHER);
    }
}
