<?php

namespace App\Form;

use App\Entity\GeneralSettings;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GeneralSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Title')
            ->add('slogan')
            ->add('imageFile', VichImageType::class,[
                'label' => 'choix du logo'
            ])
            ->add('logo', HiddenType::class)
            ->add('descriptionHome', CKEditorType::class)
            ->add('link')
            ->add('linkGit')
            ->add('linkLinkedIn')
            ->add('metaDescriptionContent', CKEditorType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GeneralSettings::class,
        ]);
    }
}
