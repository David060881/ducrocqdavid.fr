<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullName', TextType::class, [ 
                'label' => 'Nom et prénom',
                'attr' => [
                    'minlenght' => '2', 
                    'maxlenght' => '250',
                ],
                'constraints' => [ 
                    new Assert\NotBlank(),
                    new Assert\Length(['min'=>2, 'max' => 250])
                ]

            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'minlenght' => '2', 
                    'maxlenght' => '250',
                ],
                'label' => 'Adresse Email',
                'constraints' => [ 
                    new Assert\NotBlank(),
                    new Assert\Email(),
                    new Assert\Length(['min'=>2, 'max' => 180])
                ]
                
            ])
            ->add('subject', TextType::class, [ 
                'label' => 'Sujet',
                'attr' => [
                    'minlenght' => '2', 
                    'maxlenght' => '250',
                ],
                'constraints' => [ 
                    new Assert\NotBlank(),
                    new Assert\Length(['min'=>2, 'max' => 250])
                ]

            ])
            ->add('message', TextareaType::class)
            ->add('consent', CheckboxType::class, [
                'label' => 'Je consens à l\'enregistrement de mes donnés',
                'attr'=> [
                    'class' => 'checkbox'
                ]
            ])
            ->add('submit', SubmitType::class,[
                'attr'=> [
                    'class' => 'active bi bi-send'
                ]
            ])
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
