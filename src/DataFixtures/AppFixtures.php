<?php

namespace App\DataFixtures;

use App\Entity\CategoryArticle;
use App\Entity\CategoryCompetence;
use App\Entity\CategoryProject;
use App\Entity\Competence;
use App\Entity\Contact;
use App\Entity\GeneralSettings;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $generaleSettings = new GeneralSettings();
        $generaleSettings->setLink('https://ducrocqdavid.fr')
            ->setDescriptionHome("Passionné depuis toujours par l’environnement informatique, j’ai choisi à 39 ans de me reconvertir. Pour ce faire, j’ai rejoint le groupe FIM en septembre 2020, afin de réaliser un BTS développeur intégrateur de medias interactifs. ")
            ->setLogo('https://picsum.photos/200/300')
            ->setTitle('DucrocqDavid.fr')
            ->setCreatedAt(new \DateTime())
            ->setSlogan('French Dev.');
        $manager->persist($generaleSettings);
        $manager->flush();


        $categoryCompetence = new CategoryCompetence();
        $categoryCompetence->setName('Language');
        $manager->persist($categoryCompetence);
        $manager->flush();

        $competence = new Competence();
        $competence->setName('HTML5')
            ->setLevel(70)
            ->setCategoryCompetence($categoryCompetence);
        $manager->persist($competence);
        $manager->flush();


        $categoryProject = new CategoryProject();
        $categoryProject ->setName('REACT')
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
            ->setImageName('coucou');
        $manager->persist($categoryProject);
        $manager->flush();

        $project = new Project();
        $project-> setName('Video Futur')
            ->setGitRepo('lien git')
            ->setLinkDemo('lien demo')
            ->setCategoryProject($categoryProject);
        $manager->persist($project);
        $manager->flush();


        $categoryArticle = new CategoryArticle();
        $categoryArticle->setName('Defaut')
            ->setDescription('catégorie à selectionner par défaut');
        $manager->persist($categoryArticle);
        $manager->flush();

    
        //contact 

        for($i =0; $i < 5; $i++)
        { 
            $contact = new Contact();
            $contact->setFullName('rototo' .$i +1)
            ->setEmail('mail' .$i +15)
            ->setSubject('bonjour!' . $i +15)
            ->setMessage('lorem ipsum' . $i  + 5)
            ->setConsent(true);
            $manager->persist($contact);
        }
        $manager->flush();
    }
}
