<?php

namespace App\EventSubscriber;

use App\Repository\CustomPageRepository;
use Twig\Environment;
use App\Repository\GeneralSettingRepository;
use App\Repository\GeneralSettingsRepository;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TwigEventsSubscriber implements EventSubscriberInterface
{


    private  $twig;
    private  $generalSettingRepository;

    public function __construct(Environment $twig, GeneralSettingsRepository $generalSettingRepository,CustomPageRepository $customPageRepository)
    {
        $this->twig = $twig;
        $this->GeneralSettingsRepository = $generalSettingRepository;
        $this->CustomPageRepository = $customPageRepository;

    }
    public function onControllerEvent(ControllerEvent $event): void
    {
        // ...
        $this->twig->addGlobal('generalSettings', $this->GeneralSettingsRepository->findOneBy([],['id'=> 'ASC']));
        $this->twig->addGlobal('CustomPages', $this->CustomPageRepository->findAll());
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
