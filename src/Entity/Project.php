<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $gitRepo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkDemo = null;

    #[ORM\ManyToOne(inversedBy: 'projects')]
    private ?CategoryProject $categoryProject = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGitRepo(): ?string
    {
        return $this->gitRepo;
    }

    public function setGitRepo(string $gitRepo): self
    {
        $this->gitRepo = $gitRepo;

        return $this;
    }

    public function getLinkDemo(): ?string
    {
        return $this->linkDemo;
    }

    public function setLinkDemo(?string $linkDemo): self
    {
        $this->linkDemo = $linkDemo;

        return $this;
    }

    public function getCategoryProject(): ?CategoryProject
    {
        return $this->categoryProject;
    }

    public function setCategoryProject(?CategoryProject $categoryProject): self
    {
        $this->categoryProject = $categoryProject;

        return $this;
    }

}
