<?php

namespace App\Entity;

use App\Repository\CompetenceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompetenceRepository::class)]
class Competence
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?int $level = null;

    #[ORM\ManyToOne(inversedBy: 'competence')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CategoryCompetence $categoryCompetence = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCategoryCompetence(): ?CategoryCompetence
    {
        return $this->categoryCompetence;
    }

    public function setCategoryCompetence(?CategoryCompetence $categoryCompetence): self
    {
        $this->categoryCompetence = $categoryCompetence;

        return $this;
    }
}
