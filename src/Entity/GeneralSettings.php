<?php

namespace App\Entity;

use App\Repository\GeneralSettingsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: GeneralSettingsRepository::class)]
#[Vich\Uploadable]
class GeneralSettings
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $Title = null;

    #[ORM\Column(length: 255)]
    private ?string $slogan = null;

    #[Vich\UploadableField(mapping: 'logoImage', fileNameProperty: 'logo')]
    private ?File $imageFile = null;


    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $logo = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descriptionHome = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkGit = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkLinkedIn = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $metaDescriptionContent = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $servicesTitle = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $servicesContent = null;

        
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function setSlogan(string $slogan): self
    {
        $this->slogan = $slogan;

        return $this;
    }

  /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }



    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getDescriptionHome(): ?string
    {
        return $this->descriptionHome;
    }

    public function setDescriptionHome(?string $descriptionHome): self
    {
        $this->descriptionHome = $descriptionHome;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getLinkGit(): ?string
    {
        return $this->linkGit;
    }

    public function setLinkGit(?string $linkGit): self
    {
        $this->linkGit = $linkGit;

        return $this;
    }

    public function getLinkLinkedIn(): ?string
    {
        return $this->linkLinkedIn;
    }

    public function setLinkLinkedIn(?string $linkLinkedIn): self
    {
        $this->linkLinkedIn = $linkLinkedIn;

        return $this;
    }

    public function getMetaDescriptionContent(): ?string
    {
        return $this->metaDescriptionContent;
    }

    public function setMetaDescriptionContent(?string $metaDescriptionContent): self
    {
        $this->metaDescriptionContent = $metaDescriptionContent;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getServicesTitle(): ?string
    {
        return $this->servicesTitle;
    }

    public function setServicesTitle(?string $servicesTitle): self
    {
        $this->servicesTitle = $servicesTitle;

        return $this;
    }

    public function getServicesContent(): ?string
    {
        return $this->servicesContent;
    }

    public function setServicesContent(?string $servicesContent): self
    {
        $this->servicesContent = $servicesContent;

        return $this;
    }
}
